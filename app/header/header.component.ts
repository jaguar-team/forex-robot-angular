import {
  Component,
  Input,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'header',
  templateUrl: 'app/header/header.component.html',
  styleUrls: ['app/header/header.component.css']
})
export class HeaderComponent {

	public clickMessage;

	public _dropdown = 'close';

	public el;

	public isSpecial = 'cxv';

	public badCurly = 'dsf';

	dropdown(el: ElementRef)
	{
		this._dropdown = 'open';
	}
}