import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CORE_DIRECTIVES} from '@angular/common';
import {TranslatePipe, TranslateService} from "ng2-translate/ng2-translate";

import {API} from "../shared/apiservice/api";
import {SWEETALERT} from "../shared/utils/sweetalert";
import {LOCALSTORAGE} from "../shared/utils/localStorage";


@Component({
  selector: 'login',
  templateUrl: 'app/login/login.component.html',
  styleUrls  : ['app/login/login.component.css'],
  providers  : [API, SWEETALERT, LOCALSTORAGE, ],
  directives : [CORE_DIRECTIVES]
})

export class LoginComponent implements OnInit {

	// Notes.
	// Popup will be access in a different way for the next we
	// push an update.


	// --- Constants.
	// All this will be removed in the next update
	// this will be loaded in a json
	private INPUT_TITLE1_LOGIN      = "Username";
	private INPUT_MESSAGE1_LOGIN    = "Enter your Username";
	private INPUT_TITLE2_LOGIN      = "Password";
	private INPUT_MESSAGE2_LOGIN    = "Enter your password";
	private BUTTON_LOGIN            = "Login";
	private INPUT_TITLE1_PASSWORD   = "Password";
	private INPUT_MESSAGE1_PASSWORD = "Enter your new password";
	private INPUT_TITLE2_PASSWORD   = "Password";
	private INPUT_MESSAGE2_PASSWORD = "Confirm your password";
	private BUTTON_RESET_PASSWORD   = "Create a password";

	private FORGOT_PASSWORD_TOP_TITLE     = "Forgot your password?";
	private FORGOT_PASSWORD_TITLE         = "Email";
	private FORGOT_PASSWORD_MESSAGE       = "Enter your email";
	private FORGOT_PASSWORD_BACK_TO_LOGIN = "Back to login page";
	private FORGOT_PASSWORD_BUTTON_NAME   = "Send request";

	// This will be ignore END <------------
	// End constants ---


	// --- Fields.
	private hash                = false;
	private isOnFoprgotPassword = false;
	private termsAccepted       = false;
	private hashValue           = "";
	private input1              = "";
	private input2              = "";

	private uuid = '';
	// end fields ---

	/**
	 * Class constructor
	 *
	 * @param router
	 * @param api
	 * @param translate
	 * @param sweetAlert
	 * @param lcStorage
	 */
	constructor(private router:Router, private api:API, translate:TranslateService,
	            private sweetAlert:SWEETALERT, private lcStorage:LOCALSTORAGE) {
		var userLang = navigator.language.split('-')[0]; // use navigator lang if available
		userLang     = /(fr|en)/gi.test(userLang) ? userLang : 'en';
		// this language will be used as a fallback when a translation isn't found in the current language
		translate.setDefaultLang('en');
		// the lang to use, if the lang isn't available, it will use the current loader to get them
		translate.use(userLang);
	}

	/**
	 * Get the names OnInit
	 */
	ngOnInit() {
		// Initialize the field token
		this.hash                = false;
		// Initialize isOnForgotPassword
		this.isOnFoprgotPassword = false;
		// For this demo will just check if the parameter hash exist.
		this.hash                = (location.search.split('hash=')[1] == undefined) ? false : true;
		this.hashValue           = location.search.split('hash=')[1];

	}

	// --- Public methods ---

	/**
	 * Validates the user.
	 *
	 * From here we control the behavior for this button,
	 * it is used to authenticate a user; activate the account ....
	 */
	public validate() {
		if (this.hash) {
			// Check if inputs are the same.
			if (this.input1 != this.input2) this.sweetAlert.passwordError();
			// Confirm the contact user.
			else this.api.confirmContactUser(this.hash, this.input1).subscribe(
				data => this.confirmContactUserSuccess(data),
				err => this.confirmContactUserError(err)
			);
		}
		else if (!this.hash)
		// Authenticate the user
			this.api.auth(this.input1, this.input2).subscribe(
				data => this.authSuccess(data),
				err => this.authError(err))
	}

	/**
	 * Switch from the login state to forgot password,
	 * Also clear all the input files when we do this.
	 */
	public changeStates() {
		// Switch views
		this.isOnFoprgotPassword = !this.isOnFoprgotPassword;
		// Reset all inputs
		this.resetInputs();
	}

	// --- Private methods ---

	// --- Authentication

	/**
	 * Authenticate error.
	 *
	 * @param err
	 */
	private authError(err) {
		// Status 400 means the user needs to accept
		// the terms and conditions first.
		if (err.status == 400) {
			let d = JSON.parse(err._body);
			this.uuid = d.uuid;
			this.termsAndConditionsBox();
		}
		// Status 401, user can to be authenticated
		// incorrect username or password.
		else if (err.status == 401) this.sweetAlert.authError().then(() => {
			this.resetInputs();
		});
	}

	/**
	 * Authenticate success.
	 *
	 * @param result
	 */
	private authSuccess(result) {
		// Checking for now that the result contains a token
		// If so, we will store the token, then
		// display the confirm login popup.
		if (result.token != null || result.token != 'undefined') {
			this.lcStorage.setToken(result.token);
			this.sweetAlert.authSuccess().then(() => {
				// After close we are redirected to the next view.
				this.router.navigate(['/customer-detail']);
			});
		}
	}

	// Confirm Contact User

	/**
	 * Confirm contact user success.
	 *
	 * @param result
	 */
	private confirmContactUserSuccess(result) {
		if (result == true) {
			this.resetInputs();
			this.sweetAlert.confirmContactUserSuccess().then(() => {
				this.hash = false;
			});
		}
	}

	/**
	 * Confirm contact user error.
	 *
	 * @param err
	 */
	private confirmContactUserError(err) {
		this.sweetAlert.confirmContactUserError().then(() => {
			this.hash = false;
			this.resetInputs();
		});
	}


	// Accept

	/**
	 * Accept success.
	 *
	 * @param data
	 */
	private acceptSuccess(data) {
		// Authenticate the user
		this.api.auth(this.input1, this.input2).subscribe(
			data => this.authSuccess(data),
			err => this.authError(err));
	}

	/**
	 * Accept error.
	 *
	 * @param err
	 */
	private acceptError(err) {
		this.sweetAlert.acceptError();
	}

	/**
	 * Opens the terms and conditions box.
	 */
	private termsAndConditionsBox() {
		this.sweetAlert.termsAndConditionsBox().then(() => {
			this.termsAccepted = true;
			// Send request to accept the terms and conditions.
			this.api.accept(this.uuid, 'true').subscribe(
				data => this.acceptSuccess(data),
				err => this.acceptError(err)
			);
		}, (dismiss) => {
			if (dismiss === 'cancel' || dismiss === 'overlay' || dismiss === 'close')
				this.sweetAlert.termsAndConditionsError();
		}).done();
	}

	/**
	 * Reset the inputs in the html template.
	 */
	private resetInputs() {
		this.input1 = this.input2 = "";
	}
}
