import { NgModule, enableProdMode, PLATFORM_DIRECTIVES, provide } from '@angular/core';

/** @angular **/
import { Http, HTTP_PROVIDERS } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

/** @custom **/
import { ResponsiveModule } from 'ng2-responsive';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

import { routing }        from './app.routing';

import { HeaderComponent }  from './header/header.component';
import { SidebarComponent }  from './sidebar/sidebar.component';
import { FooterComponent }  from './footer/footer.component';

/** @component **/
import { AppComponent } from './app.component';
import { LoginComponent }  from './login/login.component';
import { CustomerDetailComponent }  from './customerdetail/customerdetail.component';

/** @directive **/
import { HighlightDirective } from './header/highlight.directive';





@NgModule({
  imports: [
    BrowserModule,
    FormsModule,   
    ResponsiveModule,
    TranslateModule.forRoot({ 
        provide: TranslateLoader,
        useFactory: (http: Http) => new TranslateStaticLoader(http, '/assets/i18n', '.json'),
        deps: [Http]
    }),
    routing,
  ],
  providers: [
      Http,
      HTTP_PROVIDERS
  ],
  declarations: [
    AppComponent,

    LoginComponent,

    CustomerDetailComponent,
    
    HeaderComponent,
    SidebarComponent,
    FooterComponent,

    HighlightDirective,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }