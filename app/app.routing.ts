import { Routes, RouterModule } from '@angular/router';

import { LoginComponent }  from './login/login.component';
import { CustomerDetailComponent }  from './customerdetail/customerdetail.component';

const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'customer-detail',
    component: CustomerDetailComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);