import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { SWEETALERT } from "../shared/utils/sweetalert";
import { API } from "../shared/apiservice/api";
import { LOCALSTORAGE } from "../shared/utils/localStorage";
import { CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass } from '@angular/common';
import { SELECT_DIRECTIVES } from 'ng2-select';

/**
 * This class represents the lazy loaded CustomerDetailComponent.
 */
@Component({	
	selector   : 'sd-customerdetail',
	templateUrl: 'app/customerdetail/customerdetail.component.html',
	directives : [CORE_DIRECTIVES, SELECT_DIRECTIVES, NgClass, CORE_DIRECTIVES, FORM_DIRECTIVES],
	providers  : [API, SWEETALERT, LOCALSTORAGE],
	styleUrls  : ['app/customerdetail/customerdetail.component.css']
})

export class CustomerDetailComponent implements OnInit {

	// From vocabularies
	private countries;
	private industries;
	private fatcaEntityTypes;
	public currentSelectedAddress = {};

	private SIDE = 'side';
	private MAIN = 'main';

	private sideNaOpenWidth  = '250px';
	private sideNaCloseWidth = '0';

	private isSideNavOpen = false;
	private showSummary   = false;
	private showDetails   = false;
	private showdd        = false;

	private alternativeName;
	private incorporationNumber;
	private giinNumber;
	private tinnEinNumber;
	private newAddressObj: Address;

	private draft;

	/**
	 * Creates an instance of the AdminComponent with the injected
	 * Router.
	 *
	 * @param {Router} router - The injected Router.
	 * @param sweetAlert
	 * @param api
	 * @param lcStorage
	 */
	constructor(private router:Router, private sweetAlert:SWEETALERT,
	            private api:API, private lcStorage:LOCALSTORAGE) {
	}

	/**
	 * Get the names OnInit
	 */
	ngOnInit() {
		console.log(this.lcStorage.getToken());
		// Check if we have a token in the local storage, if null or undefined
		// user will be send to the login component.
		if (!this.lcStorage.hasToken()) this.sendToLogin();
		else {
			// Trigger the method to get the vocabulary.
			this.vocabulary();
			this.showSummary = true;
		}
	}

	// --- Public methods.

	/**
	 * Log out method.
	 * If user accept to logout, it will be redirected to
	 * the login screen and the token will be removed.
	 */
	public logOut() {
		this.sweetAlert.logOutBox().then(() => {
			// Remove token
			this.lcStorage.removeToken();
			// Return to login .
			this.sendToLogin();
		}, (dismiss) => {
			if (dismiss === 'cancel' || dismiss === 'overlay' || dismiss === 'close') {
				// nothig to do stay in this component
			}
		});
	}

	/**
	 * Opens or close the side bar navigation
	 */
	public openNav() {
		this.isSideNavOpen                                  = !this.isSideNavOpen;
		document.getElementById(this.SIDE).style.width      = (this.isSideNavOpen) ? this.sideNaOpenWidth : this.sideNaCloseWidth;
		document.getElementById(this.MAIN).style.marginLeft = (this.isSideNavOpen) ? this.sideNaOpenWidth : this.sideNaCloseWidth;
	}

	// --- Side navigation buttons ---

	/**
	 * Summary button clicked.
	 * Shows the summary content.
	 */
	public summaryButtonClicked() {
		this.hideViews();
		this.showSummary = true;
	}

	/**
	 * Details button clicked.
	 * Shows the details content.
	 */
	public detailsButtonClicked() {		
		this.getDraftContact();
	}

	/**
	 * D.D. button clicked.
	 * Shows the D.D. content.
	 */
	public ddButtonClicked() {
		this.hideViews();
		this.showdd = true;
	}


	// Buttons -----
	/**
	 * Submits a new address.
	 */
	public submitNewAddress() {
		this.newAddressObj.type = "Business 1";
		this.newAddressObj.country = {id:"236", name:"United States"};
		this.newAddressObj.isPrimaryAddress = false;
		this.newAddressObj.useEuropeanPhoneMask = false;

		//console.log(this.newAddressObj);

		this.draft.addresses.push(this.newAddressObj);


		console.log(this.draft);
		this.setDraftContact()
		//[(ngModel)]="newAddressObj.telephone".type =
	}

	// --- Private methods.

	// -- Set draft contact.

	/**
	 * Proposes new contact details. If after proposing the changes the actual details change,
	 * the proposed changes will be lost. Setting a field to null will set the actual field
	 * to null when the proposed changes are applied
	 */
	private setDraftContact() {

		// This should not be done in this way.
		// Just for testing......
		// Something needs to be written.
		var a = {
			"nonIndividualName"      : "Aalendale Divestment Partnership LP",
			"alternativeName"        : "Hello",
			"type"                   : {
				"id"  : 2,
				"name": "Non-Individual"
			},
			"subType"                : {
				"id"  : 3,
				"name": "Partnership"
			},
			"inceptionDate"          : 1178607600000,
			"isCertificationReceived": true,
			"registrationCountry"    : {
				"id"  : 236,
				"name": "United States"
			},
			"incorporationNumber"    : this.incorporationNumber,
			"giinNumber"             : this.giinNumber,
			"tinnEinNumber"          : this.tinnEinNumber,
			"fatcaEntityType"        : {
				"id"  : 2,
				"name": "Active NFFE"
			},
			"businessLines"          : [
				{
					"id"  : 14,
					"name": "Automotive"
				},
				{
					"id"  : 18,
					"name": "Bars & Restaurants"
				},
				{
					"id"  : 19,
					"name": "Beer, Wine & Liquor"
				},
				{
					"id"  : 1,
					"name": "Accountants"
				},
				{
					"id"  : 6,
					"name": "Air Transport"
				}
			],
			"operationCountries"     : [
				{
					"id"  : 144,
					"name": "Mexico"
				},
				{
					"id"  : 40,
					"name": "Canada"
				},
				{
					"id"  : 236,
					"name": "United States"
				}
			],
			"residenceCountry"       : {
				"id"  : 236,
				"name": "United States"
			},
			"pep"                    : false,
			"addresses"              : [
				{
					"type"                : "Business 1",
					"isPrimaryAddress"    : true,
					"country"             : {
						"id"  : 236,
						"name": "United States"
					},
					"city"                : "Fort Lauderdale",
					"postalCode"          : "56465",
					"email"               : "pmckiernan@riskpass.com",
					"website"             : "www.company.com",
					"cell"                : "15556669899",
					"telephone"           : "",
					"fax"                 : "",
					"home"                : "",
					"useEuropeanPhoneMask": false
				},
				{
					"type"                : "Business 2",
					"isPrimaryAddress"    : false,
					"country"             : {
						"id"  : 17,
						"name": "Bahamas"
					},
					"city"                : "George Town",
					"cell"                : "14416546546",
					"telephone"           : "",
					"useEuropeanPhoneMask": false
				}
			]
		}

		// END---

		console.log(this.draft)
		this.api.setDraftContact(JSON.stringify(this.draft)).subscribe(
		//this.api.setDraftContact(a).subscribe(
			data => this.setDraftContactSuccess(data),
			err => this.setDraftContactError(err)
		);
	}

	/**
	 * Set draft contact success.
	 *
	 * @param data
	 */
	private setDraftContactSuccess(data) {
		console.log(data);
		// success do something
	}

	/**
	 * Set draft contact error.
	 *
	 * @param err
	 */
	private setDraftContactError(err) {
		console.log(err);
		// error.
	}

	// Get draft contact.

	/**
	 *
	 */
	private getDraftContact() {
		this.api.getDraftContact().subscribe(
			data => this.getDraftContactSuccess(data),
			err => this.getDraftContactError(err)
		);
	}

	/**
	 * Get draft contact success.
	 *
	 * @param data
	 */
	private getDraftContactSuccess(data) {
		console.log(data);
		this.draft = data;

		/*for (let i in data.businessLines) {
			this.items.push(data.businessLines[i].name);
		}*/

		/*for (let i in data.operationCountries) {
			this.countryOperation.push(data.operationCountries[i].name);
		}*/

		this.alternativeName     = data.alternativeName;
		this.tinnEinNumber       = data.tinnEinNumber;
		this.incorporationNumber = data.incorporationNumber;
		this.giinNumber          = data.giinNumber;

		this.hideViews();
		this.showDetails = true;
	}

	/**
	 * Get draft contact error.
	 *
	 * @param err
	 */
		private getDraftContactError(err) {
		
		let data = `javax.ws.rs.NotFoundException: Contact 1 is missing or is not accessible
	at com.riskpass.rpcjava.services.ModelService.checkMissing(ModelService.java:42)
	at com.riskpass.rpcjava.services.ModelService$$FastClassBySpringCGLIB$$c781d8d4.invoke(<generated>)
	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)
	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:650)
	at com.riskpass.rpcjava.services.ModelService$$EnhancerBySpringCGLIB$$fd0f364c.checkMissing(<generated>)
	at com.riskpass.rpcjava.services.AccessControlService.findUserPrimaryContactId(AccessControlService.java:51)
	at com.riskpass.rpcjava.cp.draft.DraftContactService.lambda$getDraftContact$485(DraftContactService.java:44)
	at com.riskpass.rpcjava.cp.draft.DraftContactService$$Lambda$382/1216430549.call(Unknown Source)
	at com.riskpass.rpcjava.services.ConnectionService.call(ConnectionService.java:114)
	at com.riskpass.rpcjava.services.DataService.lambda$isolate$252(DataService.java:65)
	at com.riskpass.rpcjava.services.DataService$$Lambda$170/1222132665.call(Unknown Source)
	at com.riskpass.rpcjava.Execution.lambda$call$110(Execution.java:71)
	at com.riskpass.rpcjava.Execution$$Lambda$156/60946963.execute(Unknown Source)
	at com.riskpass.rpcjava.Execution.exec(Execution.java:54)
	at com.riskpass.rpcjava.Execution.call(Execution.java:71)
	at com.riskpass.rpcjava.Execution$$FastClassBySpringCGLIB$$fadd5cd0.invoke(<generated>)
	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)
	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:650)
	at com.riskpass.rpcjava.Execution$$EnhancerBySpringCGLIB$$125a96c8.call(<generated>)
	at com.riskpass.rpcjava.services.DataService.isolate(DataService.java:65)
	at com.riskpass.rpcjava.cp.draft.DraftContactService.getDraftContact(DraftContactService.java:41)
	at com.riskpass.rpcjava.cp.ContactApplication.getDraftContact(ContactApplication.java:41)
	at com.riskpass.rpcjava.cp.ContactApplication$$FastClassBySpringCGLIB$$4500ea29.invoke(<generated>)
	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)
	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:718)
	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:157)
	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:85)
	at com.riskpass.rpcjava.cp.HTTPErrorHandler$$Lambda$381/1824589855.produce(Unknown Source)
	at com.riskpass.rpcjava.cp.HTTPErrorHandler.call(HTTPErrorHandler.java:30)
	at com.riskpass.rpcjava.cp.HTTPErrorHandler.lambda$handle$517(HTTPErrorHandler.java:23)
	at com.riskpass.rpcjava.cp.HTTPErrorHandler$$Lambda$380/759886946.call(Unknown Source)
	at com.riskpass.rpcjava.services.ApplicationService.handleError(ApplicationService.java:27)
	at com.riskpass.rpcjava.cp.HTTPErrorHandler.handle(HTTPErrorHandler.java:22)
	at sun.reflect.GeneratedMethodAccessor36.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:621)
	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:610)
	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:68)
	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:179)
	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)
	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:179)
	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:654)
	at com.riskpass.rpcjava.cp.ContactApplication$$EnhancerBySpringCGLIB$$8ee848a.getDraftContact(<generated>)
	at sun.reflect.GeneratedMethodAccessor35.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at org.jboss.resteasy.core.MethodInjectorImpl.invoke(MethodInjectorImpl.java:139)
	at org.jboss.resteasy.core.ResourceMethodInvoker.invokeOnTarget(ResourceMethodInvoker.java:295)
	at org.jboss.resteasy.core.ResourceMethodInvoker.invoke(ResourceMethodInvoker.java:249)
	at org.jboss.resteasy.core.ResourceMethodInvoker.invoke(ResourceMethodInvoker.java:236)
	at org.jboss.resteasy.core.SynchronousDispatcher.invoke(SynchronousDispatcher.java:376)
	at org.jboss.resteasy.core.SynchronousDispatcher.invoke(SynchronousDispatcher.java:199)
	at org.jboss.resteasy.plugins.server.servlet.ServletContainerDispatcher.service(ServletContainerDispatcher.java:221)
	at org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher.service(HttpServletDispatcher.java:56)
	at org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher.service(HttpServletDispatcher.java:51)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:790)
	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:821)
	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1685)
	at org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:207)
	at org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:176)
	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:346)
	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:262)
	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1668)
	at com.riskpass.rpcjava.TokenFilter.doFilter(TokenFilter.java:83)
	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:346)
	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:262)
	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1668)
	at org.eclipse.jetty.websocket.server.WebSocketUpgradeFilter.doFilter(WebSocketUpgradeFilter.java:225)
	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1668)
	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:581)
	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:143)
	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:548)
	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:226)
	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1158)
	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:511)
	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:185)
	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:1090)
	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:141)
	at org.eclipse.jetty.server.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:213)
	at org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:109)
	at org.eclipse.jetty.server.handler.HandlerWrapper.handle(HandlerWrapper.java:119)
	at org.eclipse.jetty.server.Server.handle(Server.java:517)
	at org.eclipse.jetty.server.HttpChannel.handle(HttpChannel.java:308)
	at org.eclipse.jetty.server.HttpConnection.onFillable(HttpConnection.java:242)
	at org.eclipse.jetty.io.AbstractConnection$ReadCallback.succeeded(AbstractConnection.java:261)
	at org.eclipse.jetty.io.FillInterest.fillable(FillInterest.java:95)
	at org.eclipse.jetty.io.SelectChannelEndPoint$2.run(SelectChannelEndPoint.java:75)
	at org.eclipse.jetty.util.thread.strategy.ExecuteProduceConsume.produceAndRun(ExecuteProduceConsume.java:213)
	at org.eclipse.jetty.util.thread.strategy.ExecuteProduceConsume.run(ExecuteProduceConsume.java:147)
	at org.eclipse.jetty.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:654)
	at org.eclipse.jetty.util.thread.QueuedThreadPool$3.run(QueuedThreadPool.java:572)
	at java.lang.Thread.run(Thread.java:745)`;
		this.getDraftContactSuccess(data);
		// Do something with the error.
	}

	/**
	 * This method is trigger to hide the views before we switch
	 * to a new one.
	 */
	private hideViews() {
		// We hide all the views and the contents.
		this.showdd = this.showDetails = this.showSummary = false;
	}

	/**
	 * Sends the application to login
	 */
	private sendToLogin() {
		this.router.navigate(['']);
	}

	/**
	 * Gets the vocabulary
	 */
	private vocabulary() {
		this.api.vocabulary().subscribe(
			data => this.vocabularySuccess(data),
			err => this.vocabularyError(err)
		);
	}

	/**
	 * Vocabulary success.
	 *
	 * @param data
	 */
	private vocabularySuccess(data:any):void {
		this.countries = data.countries;
		this.industries = data.industries;
		this.fatcaEntityTypes = data.fatcaEntityTypes;
		console.log(data);
		for (let i in data.industries) {
			this.items.push(data.industries[i].name);
		}

		for (let i in data.countries) {
			this.countryOperation.push(data.countries[i].name);
		}
	}

	/**
	 * Vocabulary error.
	 *
	 * @param err
	 */
	private vocabularyError(err:any):any {
		console.log(err);
	}


	public items:Array<string>            = [];
	public countryOperation:Array<string> = [];

	private value:any         = ['Athens'];
	private _disabledV:string = '0';
	private disabled:boolean  = false;

	private get disabledV():string {
		return this._disabledV;
	}

	private set disabledV(value:string) {
		this._disabledV = value;
		this.disabled   = this._disabledV === '1';
	}

	public selected(value:any):void {
		console.log('Selected value is: ', value);
	}

	public removed(value:any):void {
		console.log('Removed value is: ', value);
	}

	public refreshValue(value:any):void {
		this.value = value;
	}

	public itemsToString(value:Array<any> = []):string {
		return value
			.map((item:any) => {
				return item.text;
			}).join(',');
	}


	/// Manage addresses
	// Here for now, needs to be clean
	/**
	 * Edit an address.
	 *
	 * @param obj
	 */
	private binx;
	public editAddress (obj, binx){
		this.binx = binx;

		this.currentSelectedAddress = obj;
		console.log(this.currentSelectedAddress);
	}

	public editAddress2 (obj2){
		this.draft.addresses[this.binx] = this.currentSelectedAddress;
		///*console.log(obj2);
		//console.log(binx);*/
		console.log(this.draft.addresses);
	}

	/**
	 * Deletes an address.
	 *
	 * @param obj
	 */
	public deleteAddress (obj:any):void {
		//console.log(obj);
		//
		this.sweetAlert.deleteAddress().then(() => {
			var a = 0;
			console.log(this.draft.addresses.length);
			for (var i = 0; i< this.draft.addresses.length; i++) {
				console.log(this.draft.addresses[i].email);
				console.log(obj.email);

				if (this.draft.addresses[i].email == obj.email){
					console.log(i);
					a = i;
				}
			}

			console.log(a);
			//delete this.draft.addresses[a];
			this.draft.addresses.splice(a,1);

			console.log(this.draft.addresses);
			this.setDraftContact();

			//
		}, (dismiss) => {
			if (dismiss === 'cancel' || dismiss === 'overlay' || dismiss === 'close') {
				// Do nothing for now.
			}
		});
	}



	/**
	 * View an address.
	 *
	 * @param obj
	 */
	public viewAddress (obj:any):void {
		console.log(obj);
	}

}

class Address{
	type: string;
	country: {};
	isPrimaryAddress: boolean;
	useEuropeanPhoneMask: boolean;
}