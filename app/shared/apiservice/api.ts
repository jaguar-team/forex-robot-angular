import { Http, Headers, HTTP_PROVIDERS, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class API {

	// Base url
	private  BASE_URL = 'https://devrpcentral.riskpass.com:443/api/v1';

	/**
	 *  Class constructor
	 *
	 * @param http
	 */
	constructor(private http:Http) {
	}


	// --- Public methods ---

	/**
	 * Authenticates the user.
	 *
	 * @param email
	 * @param password
	 *
	 * @returns {any}
	 */
	public auth(email, password) {
		return this.http.post(this.BASE_URL + '/auth', {
				"password"    : password,
				"responseType": {"name": "string", "id": 1},
				"response"    : "string",
				"email"       : email
			}, {headers: this.getHeaders()})
			.map((res:Response) => res.json());
	}

	/**
	 * Confirm a contact user.
	 *
	 * @param hash - The hash obtained from the invitation email.
	 * @param password - The password to be set.
	 *
	 * @returns {any}
	 */
	public confirmContactUser(hash, password) {
		return this.http.put(this.BASE_URL + '/confirm-contact-user/' + hash, {
				"password"    : password,
				"responseType": {
					"name": "string",
					"id"  : 1
				},
				"response"    : "string"
			}, {headers: this.getHeaders()})
			.map((res:Response) => res.json());
	}


	/**
	 *  Accept invitation.
	 *
	 * @param uuid - UUID as returned by the previous failed /auth request.
	 * @param accepted - True if the user accepts the terms, false - otherwise.
	 *
	 * @returns {any}
	 */
	public accept(uuid, accepted) {
		return this.http.get(this.BASE_URL + '/accept?uuid=' + uuid + '&accepted=' + accepted, {headers: this.getHeaders()})
			.map((res:Response) => res.json());
	}


	/**
	 * Returns contact details. The contact details may be the actual contact
	 * data or proposed changes whatever is more recent.
	 *
	 * @returns {any}
	 */
	public getDraftContact() {
		return this.http.get(this.BASE_URL + '/cp/draft-contact', {headers: this.getHeaders()})
			.map((res:Response) => res.json());
	}

	/**
	 * Proposes new contact details. If after proposing the changes the actual details change, the proposed
	 * changes will be lost. Setting a field to null will set the actual field to null when the proposed
	 * changes are applied
	 *
	 * @param data
	 *
	 * @returns {any}
	 */
	public setDraftContact(data) {
		return this.http.put(this.BASE_URL + '/cp/draft-contact', data, {headers: this.getHeaders()})
			.map((res:Response) => res.json());
	}

	/**
	 * Returns static data (they may be different for different users)
	 *
	 * @returns {any}
	 */
	public vocabulary() {
		return this.http.get(this.BASE_URL + '/vocabulary', {headers: this.getHeaders()})
			.map((res:Response) => res.json());
	}

	// --- private methods ---


	/**
	 * Gets the headers.
	 *
	 * @returns {Headers} - The API headers.
	 */
	private  getHeaders() {
		let headers = new Headers();
		// Required headers..
		headers.append('Content-Type', 'application/json');
		headers.append('Accept', 'application/json');
		headers.append('token', localStorage.getItem('token'));
		// If we have a token, we add the
		// authorization header with it.

		return headers;
	}
}