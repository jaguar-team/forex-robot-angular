import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {TranslateService} from "ng2-translate/ng2-translate";

/**
 * CLass sweetalert manages the application popups.
 *
 */
@Injectable()
export class SWEETALERT {

	private TEXT                = 'text';
	private TYPE                = 'type';
	private HTML                = 'html';
	private CANCEL_BUTTON_TEXT  = 'cancelButtonText';
	private CONFIRM_BUTTON_TEXT = 'confirmButtonText';

	// i18n base
	private i18nAdmin    = 'admin.popups.';
	private i18nCostumer = 'costumerDetails.popups.';

	// --- Admin
	private login_success              = this.i18nAdmin + 'login-success.';
	private login_error                = this.i18nAdmin + 'login-error.';
	private password_error             = this.i18nAdmin + 'password-error.';
	private confirm_contact_success    = this.i18nAdmin + 'confirm-contact-success.';
	private confirm_contact_error      = this.i18nAdmin + 'confirm-contact-error.';
	private accept_error               = this.i18nAdmin + 'accept-error.';
	private terms_and_conditions       = this.i18nAdmin + 'terms-and-conditions.';
	private terms_and_conditions_error = this.i18nAdmin + 'terms-and-conditions-error.';

	// --- Costumer details
	private logout = this.i18nCostumer + 'logout.';

	/**
	 *
	 * @param translate
	 */
	constructor(private translate:TranslateService) {
		var userLang = navigator.language.split('-')[0]; // use navigator lang if available
		userLang     = /(fr|en)/gi.test(userLang) ? userLang : 'en';
		// this language will be used as a fallback when a translation isn't found in the current language
		translate.setDefaultLang('en');
		// the lang to use, if the lang isn't available, it will use the current loader to get them
		translate.use(userLang);
	}


	// --- Public methods ---

	// ---- Admin component ---

	// Terms and conditions error.
	/**
	 * Terms and conditions error.
	 *
	 * @returns {*}
	 */
	public termsAndConditionsError() {
		return this.sweetAlert(this.i18n(this.terms_and_conditions_error));
	}

	// Password error.
	/**
	 * Password error, this alert is displayed when passwords
	 * are not the same.
	 *
	 * @returns {*}
	 */
	public passwordError() {
		return this.sweetAlert(this.i18n(this.password_error));
	}

	// -- Confirm user.
	/**
	 * Confirm contact user error.
	 *
	 * @returns {any}
	 */
	public confirmContactUserError() {
		return this.sweetAlert(this.i18n(this.confirm_contact_error));
	}

	/**
	 * Confirm contact user success
	 *
	 * @returns {any}
	 */
	public confirmContactUserSuccess() {
		return this.sweetAlert(this.i18n(this.confirm_contact_success));
	}

	// -- Auth

	/**
	 * Authenticate success.
	 *
	 * @returns {any}
	 */
	public authSuccess() {
		return this.sweetAlert(this.i18n(this.login_success));
	}

	/**
	 * Authenticate error.
	 *
	 * @returns {any}
	 */
	public authError() {
		return this.sweetAlert(this.i18n(this.login_error));
	}

	/**
	 * Accept error.
	 *
	 * @returns {any}
	 */
	public acceptError() {
		return this.sweetAlert(this.i18n(this.accept_error));
	}

	/**
	 * Opens the terms and conditions popup
	 *
	 * @returns {any}
	 */
	public termsAndConditionsBox() {
		let i182 = this.i182(this.terms_and_conditions);
		let html = '<div style="height:300px;width:auto;overflow:auto;color:black;scrollbar-base-color:black;' +
			'font-family:sans-serif;padding:10px;font-size: 12px">' + i182.html + '</div>';
		return swal({
			title            : i182.text,
			html             : html,
			showCloseButton  : true,
			showCancelButton : true,
			confirmButtonText: i182.confirmButtonText,
			cancelButtonText : i182.cancelButtonText
		});
	}


	// --- Costumer details ---

	// --- logout
	/**
	 * Open the logout box popup, the user will be ask if he
	 * want's to continue or not.
	 *
	 * @returns {any}
	 */
	public logOutBox() {
		let i182 = this.i182(this.logout);
		return swal({
			title            : i182.text,
			type             : 'warning',
			showCloseButton  : true,
			showCancelButton : true,
			confirmButtonText: i182.confirmButtonText,
			cancelButtonText : i182.cancelButtonText
		});
	}

	// Manage addresses
	public deleteAddress () {
		return swal({
			title            : 'Do you want to remove this address?',
			type             : 'warning',
			showCloseButton  : true,
			showCancelButton : true,
			confirmButtonText: 'Yes',
			cancelButtonText : 'Cancel'
		});
	}


	// --- private methods ---

	/**
	 * Open the sweet alert.
	 *
	 * @param i18n
	 *
	 * @returns {any}
	 */
	private sweetAlert(i18n) {
		return swal({
			text             : i18n.text,
			type             : i18n.type,
			confirmButtonText: i18n.confirmButtonText
		}).done();
	}

	/**
	 *
	 * @param path - The path in i18n
	 *
	 * @returns {{text: *, type: *, confirmButtonText: *}}
	 */
	private i18n(path) {
		return {
			text             : this.get_translation(path + this.TEXT),
			type             : this.get_translation(path + this.TYPE),
			confirmButtonText: this.get_translation(path + this.CONFIRM_BUTTON_TEXT)
		}
	}

	/**
	 *
	 * @param path - The path in i18n
	 *
	 * @returns {{text: *, html: *, confirmButtonText: *, cancelButtonText: *}}
	 */
	private i182(path) {
		return {
			text             : this.get_translation(path + this.TEXT),
			html             : this.get_translation(path + this.HTML),
			confirmButtonText: this.get_translation(path + this.CONFIRM_BUTTON_TEXT),
			cancelButtonText : this.get_translation(path + this.CANCEL_BUTTON_TEXT)
		}
	}

	/**
	 *
	 * @param path - The path to translate.
	 *
	 * @returns The translated value if is not null else returns an empty string
	 */
	private get_translation(path) {
		return this.translate.get(path).value;
	}
}
