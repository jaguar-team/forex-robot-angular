
export class LOCALSTORAGE {

	private TOKEN = 'token'


	// --- localStorage token
	/**
	 * Sets the token.
	 *
	 * @param token - The token to be set.
	 */
	public setToken(token) {
		localStorage.setItem(this.TOKEN, token);
	}

	/**
	 * Gets the token.
	 *
	 * @returns {string} - The token
	 */
	public getToken() {
		return localStorage.getItem(this.TOKEN);
	}

	/**
	 * Removes the token.
	 */
	public removeToken() {
		localStorage.removeItem(this.TOKEN);
	}

	/**
	 * Check if there is a token in the localStorage
	 *
	 * @returns {boolean} - True if there is a token otherwise false.
	 */
	public hasToken() {
		return (localStorage.getItem(this.TOKEN) != null || localStorage.getItem(this.TOKEN) != undefined);
	}
}