/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function(global) {
  // map tells the System loader where to look for things
  var map = {
    'app':                              'app', // 'dist',
    '@app':                             'node_modules',
    '@angular':                         'node_modules/@angular',
    'angular2-in-memory-web-api':       'node_modules/angular2-in-memory-web-api',
    'ng2-responsive':                   'node_modules/ng2-responsive',
    'ng2-translate':                    'node_modules/ng2-translate',
    'rxjs':                             'node_modules/rxjs',
    'ng2-bootstrap':                    'node_modules/ng2-bootstrap',
    'ng2-select':                       'node_modules/ng2-select',
    'moment':                           'node_modules/moment',
  };
  // packages tells the System loader how to load when no filename and/or no extension
  var packages = {
    'app':                            { main: 'main.js',  defaultExtension: 'js' },
    'rxjs':                           { defaultExtension: 'js' },
    'angular2-in-memory-web-api':     { main: 'index.js', defaultExtension: 'js' },
    'ng2-responsive':                 { main: 'index.js', defaultExtension: 'js' },
    'ng2-translate':                  { main: 'ng2-translate.js', defaultExtension: 'js' },
    'ng2-bootstrap':                  { main: 'ng2-bootstrap.js', defaultExtension: 'js' },
    'ng2-select':                     { main: 'ng2-select.js', defaultExtension: 'js' },
    'moment':                         { main: 'moment.js', defaultExtension: 'js' },

  };
  var ngPackageNames = [
    'common',
    'compiler',
    'core',
    'forms',
    'http',
    'platform-browser',
    'platform-browser-dynamic',
    'router',
    'router-deprecated',
    'upgrade',
  ];
  // Individual files (~300 requests):
  function packIndex(pkgName) {
    packages['@angular/'+pkgName] = { main: 'index.js', defaultExtension: 'js' };
  }
  // Bundled (~40 requests):
  function packUmd(pkgName) {
    packages['@angular/'+pkgName] = { main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
  }
  // Most environments should use UMD; some (Karma) need the individual index files
  var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;
  // Add package entries for angular packages
  ngPackageNames.forEach(setPackageConfig);
  var config = {
    map: map,
    packages: packages,
  };
  System.config(config);
})(this);